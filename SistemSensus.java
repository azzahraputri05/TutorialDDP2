import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author ....., NPM ....., Kelas ....., GitLab Account: .....
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		short panjang = Short.parseShort(input.nextLine());
		System.out.print("Lebar Tubuh (cm)       : ");
		short lebar = Short.parseShort(input.nextLine());
		System.out.print("Tinggi Tubuh (cm)      : ");
		short tinggi = Short.parseShort(input.nextLine());
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = Float.parseFloat(input.nextLine());
		System.out.print("Jumlah Anggota Keluarga: ");
		byte makanan = Byte.parseByte(input.nextLine());
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		byte jumlahCetakan = Byte.parseByte(input.nextLine());


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		int rasio = (int)(berat/((panjang*lebar*tinggi)*0.000001)); 
		//dikali 0.000001 soalnya kalo dibagi 100(buat convert dari cm ke m) nanti dibuletin kebawah karena dia integer
		//dan kalo gitu hasilnya jadi nggak akurat. trus ada int didepan karena hasil operasi math nya itu float, jadi harus diubah ke integer lagi

		for (int cetak=1 ; cetak<=jumlahCetakan ; cetak++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + cetak + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			String catatan2 = "";
			if (catatan.equals("")) 
				catatan2 = "Tidak ada catatan tambahan";
			else 
				catatan2 = "Catatan: " + catatan;

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			System.out.format("DATA SIAP DICETAK UNTUK %s", penerima);
			System.out.println("-----------------------------------");
			System.out.format("%s - %s\nLahir pada tanggal %s\nRasio Berat Per Volume = %skg/m3\n%s\n\n",nama,alamat,tanggalLahir,rasio,catatan2);
		}


/*		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)



		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = "";

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		... anggaran = (...) (...);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		... tahunLahir = .....; // lihat hint jika bingung
		... umur = (...) (...);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)





		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "";
		.....;

		input.close();*/
	}
}
